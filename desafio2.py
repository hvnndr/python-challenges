#IMPORTANDO
import os
import requests
import shutil
import yagmail


urls = []
#GERANDO LISTA DE URLS
def list_generator():
    results = requests.get('https://jsonplaceholder.typicode.com/photos')
    for index in results.json():
        urls.append(index['url'])

#CRIANDO DIRETÓRIO, BAIXANDO DE IMAGENS E ZIPANDO
def create_files(img_numbers):
    os.mkdir('pasta_imagens')
    for index in range(img_numbers):
        imagem = open(f'./pasta_imagens/imagem {index+1}.jpg', 'wb')
        response = requests.get(urls[index])
        imagem.write(response.content)
        imagem.close()
    shutil.make_archive('imagens', 'zip', './', './pasta_imagens')

#ENVIANDO EMAIL
def config_email():
    sender = input('Digite o email do REMETENTE: ')
    password = input("Senha: ")
    rec = input('Digite o email do DESTINATÁRIO: ')
    yag = yagmail.SMTP(user=sender, password= password)
    yag.send(to=rec, subject='DESAFIO 2 - Obrigado Pai Google', contents='Segue arquivo .zip de imagens baixadas', attachments="imagens.zip")
    print("Email enviado.")

#DELTANDO PASTAS
def delete_files():
    os.system(f'rm -rf ./pasta_imagens')
    os.system(f"rm -rf ./imagens.zip")
    print("Arquivos deletados")

#RUNNING CODE
list_generator()
create_files(5)
config_email()
delete_files()

