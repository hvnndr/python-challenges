import requests
import smtplib
from email.message import EmailMessage


results = requests.get('https://jsonplaceholder.typicode.com/users')
names = []
for index in results.json():
    names.append((index)['name'])

port = 587
smtp_server = "smtp.gmail.com"
sender= input('Digite o email do remetente: ')
password = input('Digite a senha: ')
rec = input('Digite o email do destinatário: ')

subject = "Desafio 1"
msg = 'Subject: {}\n\n{}'.format(subject, ', '.join(names))

server = smtplib.SMTP(host=smtp_server, port=port)
server.starttls()
server.login(sender, password)
server.sendmail(sender, rec, msg)
server.quit()
