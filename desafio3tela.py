import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QLabel, QHBoxLayout, QLineEdit, QPushButton, QWidget, QVBoxLayout
from desafio3 import conn, cur

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle('Funcionário')
        self.setGeometry(550,280,800,500)

        self.label_name = QLabel()
        self.label_name.setText('Nome:')
        self.edit_name = QLineEdit()

        self.label_gender = QLabel()
        self.label_gender.setText('Sexo:\n Digite apenas "M" ou "F"')
        self.edit_gender = QLineEdit()

        self.label_salary = QLabel()
        self.label_salary.setText('Salário:')
        self.edit_salary = QLineEdit()

        self.label_birth = QLabel()
        self.label_birth.setText('Data de Aniversário:')
        self.edit_birth = QLineEdit()

        self.btn_register = QPushButton("Cadastrar")
        self.btn_register.setToolTip('Enviar cadastro de funcionário')
        self.btn_register.clicked.connect(self._add_on)

        # self.label_confirm = QLabel()



        top = QHBoxLayout()
        top.addWidget(self.label_name)
        top.addWidget(self.edit_name)
        top.addWidget(self.label_gender)
        top.addWidget(self.edit_gender)

        bottom = QHBoxLayout()

        bottom.addWidget(self.label_salary)
        bottom.addWidget(self.edit_salary)
        bottom.addWidget(self.label_birth)
        bottom.addWidget(self.edit_birth)

        vbox = QVBoxLayout()

        vbox.addLayout(top)
        vbox.addLayout(bottom)
        vbox.addWidget(self.btn_register)
        # vbox.addWidget(self.label_confirm)


        panel = QWidget()

        panel.setLayout(vbox)
        self.setCentralWidget(panel)

    # def add_on(self):
    #     self.name = self.edit_name.text()
    #     self.gender = self.edit_gender.text()
    #     self.salary = self.edit_salary.text()
    #     self.insert_employee(self.name, self.gender, self.salary)

    def _add_on(self):
        name = self.edit_name.text().capitalize()
        gender = self.edit_gender.text().upper()
        salary = self.edit_salary.text()
        cur.execute('INSERT INTO employee(name, gender, salary) VALUES(%s, %s, %s)', (name, gender, salary))
        conn.commit()
        self.edit_name.setText('')
        self.edit_gender.setText('')
        self.edit_salary.setText('')
        # self.label_confirm.setText('Cadastrado!')



App = QApplication(sys.argv)
window = MainWindow()
window.show()
sys.exit(App.exec())
