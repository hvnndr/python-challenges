import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QLineEdit, QWidget, QHBoxLayout, QVBoxLayout, QPushButton

class Janela(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setGeometry(750,180,500,800)
        self.setWindowTitle("Janelinha Top")

        label_name = QLabel("Nome:")
        edit_name = QLineEdit()
        label_gender = QLabel("Sexo:")
        edit_gender = QLineEdit()
        label_salary = QLabel("Salário;")
        edit_salary = QLineEdit()
        label_birth = QLabel("Data de aniversário:")
        edit_birth = QLineEdit()
        btn_register = QPushButton("Cadastrar")

        top = QHBoxLayout()
        top.addWidget(label_name)
        top.addWidget(edit_name)
        top.addWidget(label_gender)
        top.addWidget(edit_gender)

        bottom = QHBoxLayout()
        bottom.addWidget(label_salary)
        bottom.addWidget(edit_salary)
        bottom.addWidget(label_birth)
        bottom.addWidget(edit_birth)

        vbox = QVBoxLayout()
        vbox.addLayout(top)
        vbox.addLayout(bottom)
        vbox.addWidget(btn_register)

        panel = QWidget()
        panel.setLayout(vbox)
        self.setCentralWidget(panel)


App = QApplication(sys.argv)
j1 = Janela()
j1.show()
sys.exit(App.exec_())




